export interface DesignToken {
    name: string;
    value: string;
    compiledValue: string;
}

export interface MappedDesignToken extends DesignToken {
    mapValue?: DesignToken[];
}