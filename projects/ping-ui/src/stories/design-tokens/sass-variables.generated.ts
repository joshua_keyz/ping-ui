/* tslint:disable */
export const SCSS_VARIABLES = 
{ typography: [
  {
    "name": "$base-font-size",
    "value": "1rem",
    "compiledValue": ""
  },
  {
    "name": "$font-size-sm",
    "value": "$base-font-size * 0.75",
    "compiledValue": ""
  },
  {
    "name": "$font-size-lg",
    "value": "$base-font-size * 1.5",
    "compiledValue": ""
  },
  {
    "name": "$pui-main-font-family",
    "value": "'DM Sans', sans-serif",
    "compiledValue": ""
  },
  {
    "name": "$pui-heading-font-family",
    "value": "$pui-main-font-family",
    "compiledValue": ""
  }
], sizes: [
  {
    "name": "$pui-spacing-xxxxlarge",
    "value": "80px",
    "compiledValue": "80px"
  },
  {
    "name": "$pui-spacing-xxxlarge",
    "value": "72px",
    "compiledValue": "72px"
  },
  {
    "name": "$pui-spacing-xxlarge",
    "value": "64px",
    "compiledValue": "64px"
  },
  {
    "name": "$pui-spacing-xlarge",
    "value": "56px",
    "compiledValue": "56px"
  },
  {
    "name": "$pui-spacing-large",
    "value": "48px",
    "compiledValue": "48px"
  },
  {
    "name": "$pui-spacing-medium",
    "value": "40px",
    "compiledValue": "40px"
  },
  {
    "name": "$pui-spacing-small",
    "value": "32px",
    "compiledValue": "32px"
  },
  {
    "name": "$pui-spacing-xsmall",
    "value": "24px",
    "compiledValue": "24px"
  },
  {
    "name": "$pui-spacing-xxsmall",
    "value": "16px",
    "compiledValue": "16px"
  },
  {
    "name": "$pui-spacing-xxxsmall",
    "value": "8px",
    "compiledValue": "8px"
  }
], colors: [
  {
    "name": "$pui-primary",
    "value": "#092c4c",
    "compiledValue": "#092c4c"
  },
  {
    "name": "$pui-primary-100",
    "value": "#d1eeff",
    "compiledValue": "#d1eeff"
  },
  {
    "name": "$pui-primary-200",
    "value": "#336796",
    "compiledValue": "#336796"
  },
  {
    "name": "$pui-primary-300",
    "value": "#092c4c",
    "compiledValue": "#092c4c"
  },
  {
    "name": "$pui-primary-400",
    "value": "#071f34",
    "compiledValue": "#071f34"
  },
  {
    "name": "$pui-secondary",
    "value": "#f2994a",
    "compiledValue": "#f2994a"
  },
  {
    "name": "$pui-secondary-100",
    "value": "#fff5ec",
    "compiledValue": "#fff5ec"
  },
  {
    "name": "$pui-secondary-200",
    "value": "#f3ba88",
    "compiledValue": "#f3ba88"
  },
  {
    "name": "$pui-secondary-300",
    "value": "#f2994a",
    "compiledValue": "#f2994a"
  },
  {
    "name": "$pui-secondary-400",
    "value": "#c67a36",
    "compiledValue": "#c67a36"
  },
  {
    "name": "$pui-info",
    "value": "#2f80ed",
    "compiledValue": "#2f80ed"
  },
  {
    "name": "$pui-success",
    "value": "#27ae60",
    "compiledValue": "#27ae60"
  },
  {
    "name": "$pui-warning",
    "value": "#ecb93b",
    "compiledValue": "#ecb93b"
  },
  {
    "name": "$pui-error",
    "value": "#eb5757",
    "compiledValue": "#eb5757"
  },
  {
    "name": "$btn-variants",
    "value": "( \"primary\": $pui-primary, \"danger\": $pui-error, \"secondary\": $pui-secondary )",
    "mapValue": [
      {
        "name": "primary",
        "value": "$pui-primary",
        "compiledValue": "#092c4c"
      },
      {
        "name": "danger",
        "value": "$pui-error",
        "compiledValue": "#eb5757"
      },
      {
        "name": "secondary",
        "value": "$pui-secondary",
        "compiledValue": "#f2994a"
      }
    ],
    "compiledValue": "(\"primary\": #092c4c, \"danger\": #eb5757, \"secondary\": #f2994a)"
  },
  {
    "name": "$pui-gray-4",
    "value": "#4f4f4f",
    "compiledValue": "#4f4f4f"
  },
  {
    "name": "$pui-gray-3",
    "value": "#828282",
    "compiledValue": "#828282"
  },
  {
    "name": "$pui-gray-2",
    "value": "#bdbdbd",
    "compiledValue": "#bdbdbd"
  },
  {
    "name": "$pui-gray-1",
    "value": "#ededed",
    "compiledValue": "#ededed"
  },
  {
    "name": "$pui-black",
    "value": "#000",
    "compiledValue": "#000"
  },
  {
    "name": "$pui-white",
    "value": "#fff",
    "compiledValue": "#fff"
  },
  {
    "name": "$colors",
    "value": "( \"primary\": $pui-primary, \"secondary\": $pui-secondary, \"info\": $pui-info, \"error\": $pui-error, \"success\": $pui-success, \"warning\": $pui-warning, \"black\": $pui-black, \"white\": $pui-white, )",
    "mapValue": [
      {
        "name": "primary",
        "value": "$pui-primary",
        "compiledValue": "#092c4c"
      },
      {
        "name": "secondary",
        "value": "$pui-secondary",
        "compiledValue": "#f2994a"
      },
      {
        "name": "info",
        "value": "$pui-info",
        "compiledValue": "#2f80ed"
      },
      {
        "name": "error",
        "value": "$pui-error",
        "compiledValue": "#eb5757"
      },
      {
        "name": "success",
        "value": "$pui-success",
        "compiledValue": "#27ae60"
      },
      {
        "name": "warning",
        "value": "$pui-warning",
        "compiledValue": "#ecb93b"
      },
      {
        "name": "black",
        "value": "$pui-black",
        "compiledValue": "#000"
      },
      {
        "name": "white",
        "value": "$pui-white",
        "compiledValue": "#fff"
      }
    ],
    "compiledValue": "(\"primary\": #092c4c, \"secondary\": #f2994a, \"info\": #2f80ed, \"error\": #eb5757, \"success\": #27ae60, \"warning\": #ecb93b, \"black\": #000, \"white\": #fff)"
  },
  {
    "name": "$pui-danger-100",
    "value": "#fff2f2",
    "compiledValue": "#fff2f2"
  },
  {
    "name": "$pui-danger-200",
    "value": "#ff9797",
    "compiledValue": "#ff9797"
  },
  {
    "name": "$pui-danger-300",
    "value": "#eb5757",
    "compiledValue": "#eb5757"
  },
  {
    "name": "$pui-danger-400",
    "value": "#ae0000",
    "compiledValue": "#ae0000"
  },
  {
    "name": "$pui-success-100",
    "value": "#c7ead5",
    "compiledValue": "#c7ead5"
  },
  {
    "name": "$pui-success-200",
    "value": "#8aebb3",
    "compiledValue": "#8aebb3"
  },
  {
    "name": "$pui-success-300",
    "value": "#27ae60",
    "compiledValue": "#27ae60"
  },
  {
    "name": "$pui-success-400",
    "value": "#008036",
    "compiledValue": "#008036"
  },
  {
    "name": "$pui-warning-100",
    "value": "#ffeebc",
    "compiledValue": "#ffeebc"
  },
  {
    "name": "$pui-warning-200",
    "value": "#f9dc85",
    "compiledValue": "#f9dc85"
  },
  {
    "name": "$pui-warning-300",
    "value": "#e2b93b",
    "compiledValue": "#e2b93b"
  },
  {
    "name": "$pui-warning-400",
    "value": "#bf9000",
    "compiledValue": "#bf9000"
  },
  {
    "name": "$pui-info-100",
    "value": "#deecff",
    "compiledValue": "#deecff"
  },
  {
    "name": "$pui-info-200",
    "value": "#b2d3ff",
    "compiledValue": "#b2d3ff"
  },
  {
    "name": "$pui-info-300",
    "value": "#2f80ed",
    "compiledValue": "#2f80ed"
  },
  {
    "name": "$pui-info-400",
    "value": "#013478",
    "compiledValue": "#013478"
  }
]};
