import { Component, Input } from '@angular/core';

@Component({
  selector: 'pui-logo',
  templateUrl: './logo.component.html',
  styleUrls: ['./logo.component.scss'],
})
export class PUILogoComponent {
  @Input() type: 'full' | 'icon';
  baseUrl = 'https://pinglink-space.s3.eu-north-1.amazonaws.com/pinglink-assets';

  @Input() height = 32;

  getLogoLink() {
    if (this.type === 'full') {
      return `${this.baseUrl}/logo.svg`;
    }
    return `${this.baseUrl}/icon-logo.svg`;
  }
}
