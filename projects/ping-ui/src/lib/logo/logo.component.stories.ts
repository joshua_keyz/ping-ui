import {
  Meta,
  StoryObj,
  componentWrapperDecorator,
  moduleMetadata,
} from '@storybook/angular';
import { PUILogoComponent } from './logo.component';

export default {
  title: 'Components/Logo',
  component: PUILogoComponent,
  tags: ['autodocs'],
  decorators: [
    moduleMetadata({
      declarations: [],
    }),
    componentWrapperDecorator(
      (story) => `<div style="margin: 3em">${story}</div>`
    ),
  ],
  argTypes: {
    type: {
      control: 'select',
      options: ['full', 'icon'],
    },
  },
} as Meta<PUILogoComponent>;

type Story = StoryObj<PUILogoComponent>;

export const IconLogo: Story = {
  args: {
    type: 'icon',
    height: 32,
  },
};

export const FullLogo: Story = {
  args: {
    type: 'full',
    height: 32,
  },
};
export const WithDefinedHeight: Story = {
  args: {
    type: 'full',
    height: 64,
  },
};
