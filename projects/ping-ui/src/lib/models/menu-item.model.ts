export interface MenuItemModel {
  icon: string;
  item: string;
  link: string;
}