export interface NotificationModel {
  variant: 'primary' | 'secondary' | 'danger';
  content: string;
  timeout?: number;
}