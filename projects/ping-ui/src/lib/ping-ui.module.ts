import { NgModule } from '@angular/core';
import { PingUiComponent } from './ping-ui.component';
import { PLButtonComponent } from './button/button.component';
import { PLIconComponent, PLLinkComponent, PUILayoutComponent, PUILogoComponent } from '../public-api';
import { FormFieldComponent } from './form-field/form-field.component';
import { PLAlertComponent } from './alert/alert.component';
import { RouterModule } from '@angular/router';
import { PUISelectComponent } from './select/select.component';
import { PUIProfileComponent } from './profile/profile.component';
import { ReactiveFormsModule } from '@angular/forms';
import { PUITableComponent } from './table/table.component';
import { PUISpinnerComponent } from './spinner/spinner.component';
import { CommonModule } from '@angular/common';
import { PUIImageCropperComponent } from './img-cropper/img-cropper.component';
import { PUITextEditComponent } from './text-edit/text-edit.component';


@NgModule({
  declarations: [
    PingUiComponent,
    PLButtonComponent,
    PLIconComponent,
    PLAlertComponent,
    FormFieldComponent,
    PLLinkComponent,
    PUILogoComponent,
    PUISelectComponent,
    PUILayoutComponent,
    PUIProfileComponent,
    PUITableComponent,
    PUISpinnerComponent,
    PUIImageCropperComponent,
    PUITextEditComponent,
  ],
  providers: [],
  imports: [
    RouterModule,
    CommonModule,
    ReactiveFormsModule,
  ],
  exports: [
    PingUiComponent,
    PLButtonComponent,
    PLIconComponent,
    FormFieldComponent,
    PLAlertComponent,
    PLLinkComponent,
    PUILogoComponent,
    PUISelectComponent,
    PUILayoutComponent,
    PUIProfileComponent,
    PUITableComponent,
    PUISpinnerComponent,
    PUIImageCropperComponent,
    PUITextEditComponent,
  ]
})
export class PingUiModule { }
