import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Optional,
  Output,
} from '@angular/core';
import { MenuItemModel } from '../models/menu-item.model';
import { Router } from '@angular/router';
import { v4 as uuid } from 'uuid';
import {
  trigger,
  transition,
  style,
  animate,
  query,
} from '@angular/animations';
import { NotificationService } from '../services/notification/notification.service';
import { NotificationModel } from '../models/notification.model';

@Component({
  selector: 'pui-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss'],
  animations: [
    trigger('alert', [
      transition(':enter', [
        style({
          right: '-500px',
        }),
        animate('500ms', style({ right: '0px' })),
      ]),
      transition(':leave', [animate('500ms', style({ right: '-500px' }))]),
    ]),
  ],
})
export class PUILayoutComponent implements OnInit {
  @Input() menuItems: MenuItemModel[];
  alerts = [];
  @Input() profileName: string;
  @Input() profileMenu: MenuItemModel[];
  @Input() profilePic: string;
  @Input() subscription: string;
  @Output() logout = new EventEmitter<void>();

  mobileSidebarOpen = false;
  constructor(
    private router: Router,
    @Optional() private notificationService: NotificationService
  ) {}
  routeToLink(link: string) {
    this.router.navigate([link]);
  }
  toggleMobileSidebar() {
    this.mobileSidebarOpen = !this.mobileSidebarOpen;
  }
  trigger() {
    this.notificationService.sendNotification({
      variant: 'primary',
      timeout: 500000,
      content: 'Opps!',
    });
  }
  getHamburgerIcon(): string {
    return this.mobileSidebarOpen ? 'icon-close' : 'icon-hamburger-menu';
  }

  ngOnInit(): void {
    this.notificationService?.notification$.subscribe((notificationData) => {
      this.showAlert(notificationData);
    });
    // this.trigger();
  }
  popAlert(index: number) {
    this.alerts.splice(index, 1);
  }
  showAlert(notificationData: NotificationModel) {
    const id = uuid();
    this.alerts.push({
      id,
      ...notificationData,
    });
    setTimeout(() => {
      const alertIdx = this.alerts.findIndex((alert) => alert.uuid === id);
      if (alertIdx) {
        this.alerts.splice(alertIdx, 1);
      }
    }, notificationData.timeout);
  }
}
