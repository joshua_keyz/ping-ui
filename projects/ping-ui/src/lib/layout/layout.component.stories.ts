import {
  Meta,
  StoryObj,
  componentWrapperDecorator,
  moduleMetadata,
} from '@storybook/angular';
import { PUILayoutComponent } from './layout.component';
import { PUILogoComponent } from '../logo/logo.component';
import { PLIconComponent } from '../icon/icon.component';
import { Component } from '@angular/core';
import { RouterModule } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { PLButtonComponent } from '../button/button.component';
import { PUIProfileComponent } from '../profile/profile.component';
import { PLAlertComponent } from '../alert/alert.component';
import { NotificationService } from '../../public-api';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@Component({
  template: 'Route One',
})
export class RouteOneComponent {
  
}
@Component({
  template: 'Route Two',
})
export class RouteTwoComponent {

}
@Component({
  template: 'Route Three',
})
export class RouteThreeComponent {

}

const meta: Meta<PUILayoutComponent> = {
  title: 'Components/Layout',
  component: PUILayoutComponent,
  tags: ['autodocs'],
  decorators: [
    moduleMetadata({
      providers: [NotificationService],
      declarations: [
        PUILogoComponent,
        PLIconComponent,
        RouteOneComponent,
        RouteTwoComponent,
        RouteThreeComponent,
        PLButtonComponent,
        PUIProfileComponent,
        PLAlertComponent,
      ],
      imports: [
        RouterModule,
        BrowserAnimationsModule,
        RouterTestingModule.withRoutes([
          {
            path: 'route1',
            component: RouteOneComponent,
          },
          {
            path: 'route2',
            component: RouteTwoComponent,
          },
          {
            path: 'route3',
            component: RouteThreeComponent,
          },
        ]),
      ],
    }),
    componentWrapperDecorator(
      (story) => `<div style="margin: 0em;">${story}</div>`
    ),
  ],
};
export default meta;

type Story = StoryObj<PUILayoutComponent>;

export const Default: Story = {
  args: {
    profilePic: 'https://images.unsplash.com/photo-1689613303199-90769f975895?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=715&q=80',
    profileName: 'Billy Graham',
    profileMenu: [{
      icon: 'icon-account-box',
      item: 'Account',
      link: '/route1'
    },{
      icon: 'icon-notification-bell',
      item: 'Notifications',
      link: 'route2',
    }],
    menuItems: [{
      icon: 'icon-web',
      item: 'Checks',
      link: '/route1',
    },{
      icon: 'icon-notification-bell',
      item: 'Notifications',
      link: 'route2',
    },{
      icon: 'icon-account-box',
      item: 'My Account',
      link: 'route3',
    },],
    subscription: 'freemium'
  },
  render: args => ({
    props: {
      ...args,
    },
    template: `
      <pui-layout 
        [menuItems]="menuItems" 
        [profileMenu]="profileMenu"
        [profileName]="profileName"
        [profilePic]="profilePic"
        [subscription]="subscription"
      >
        <router-outlet></router-outlet>
      </pui-layout>
    `
  })
};
