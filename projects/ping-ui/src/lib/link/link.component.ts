import { Component, Input } from '@angular/core';
import { RouterLink } from '@angular/router';
import { LinkSize } from './link.model';

@Component({
  selector: 'pui-link',
  templateUrl: './link.component.html',
  styleUrls: ['./link.component.scss'],
})
export class PLLinkComponent {
  @Input() link: string | string[];
  @Input() size: LinkSize;
  @Input() variant: 'primary' | 'secondary' = 'primary';

  get classes() {
    const classes = [];
    classes.push(`link--${this.size}`);
    classes.push(`link--${this.variant}`);
    return classes;
  }
}
