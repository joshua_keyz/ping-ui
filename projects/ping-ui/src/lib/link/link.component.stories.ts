import {
  StoryObj,
  componentWrapperDecorator,
  moduleMetadata,
} from '@storybook/angular';
import { PLLinkComponent } from './link.component';
import { RouterModule } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';

export default {
  title: 'Components/Link',
  component: PLLinkComponent,
  tags: ['autodocs'],
  decorators: [
    moduleMetadata({
      declarations: [],
      imports: [RouterTestingModule],
    }),
    componentWrapperDecorator(
      (story) => `<div style="margin: 3em">${story}</div>`
    ),
  ],
  argTypes: {
    size: {
      control: 'select',
      options: ['sm', 'md', 'lg'],
    },
    variant: {
      control: 'select',
      options: ['primary', 'secondary'],
    },
  },
};

type Story = StoryObj<PLLinkComponent>;

export const DefaultLink: Story = {
  args: {
    size: 'sm',
    link: ['/my-custom-link'],
    variant: 'primary',
  },
  render: (args) => ({
    props: args,
    template:
      '<pui-link [size]="size" [variant]="variant" [link]="link">My Link</pui-link>',
  }),
};
