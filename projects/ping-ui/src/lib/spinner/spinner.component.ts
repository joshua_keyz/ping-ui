import { Component } from "@angular/core";

@Component({
  selector: 'pui-spinner',
  templateUrl: './spinner.component.html',
  styleUrls: ['./spinner.component.scss'],
})
export class PUISpinnerComponent {}