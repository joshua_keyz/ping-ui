import { Meta, StoryObj, componentWrapperDecorator, moduleMetadata } from '@storybook/angular';
import { PUISpinnerComponent } from './spinner.component';

const meta: Meta<PUISpinnerComponent> = {
    title: 'Components/Spinner',
    component: PUISpinnerComponent,
    tags: ['autodocs'],
    decorators: [
        moduleMetadata({
            declarations: []
        }),
        componentWrapperDecorator((story) => `<div style="margin: 3em">${story}</div>`)
    ],
};

export default meta;

type IconStory = StoryObj<PUISpinnerComponent>

export const Default: IconStory = {
    args: {
        iconName: 'icon-close'
    }
};