import { Component, Inject, Injector, Input, OnInit } from '@angular/core';
import { OptionsModel, SelectSizes } from './select.model';
import { ControlValueAccessor, FormControl, FormControlDirective, FormControlName, FormGroupDirective, NG_VALUE_ACCESSOR, NgControl } from '@angular/forms';

@Component({
  selector: 'pui-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: PUISelectComponent,
      multi: true,
    },
  ],
})
export class PUISelectComponent<T> implements ControlValueAccessor, OnInit {
  @Input() multiple = false;
  onChange;
  control: FormControl | undefined;
  @Input() options: T[];
  @Input() size: SelectSizes;
  @Input() label: string;
  selected: any;

  isOpened = false;
  constructor(@Inject(Injector) private injector: Injector) { }

  ngOnInit(): void {
    this.setFormControl();
    
  }
  setFormControl() {
    try {
      const formControl = this.injector.get(NgControl);
      switch(formControl.constructor) {
        case FormControlName:
            this.control = this.injector.get(FormGroupDirective).getControl(formControl as FormControlName);
            break;
        default:
          this.control = (formControl as FormControlDirective).form as FormControl;
          break;
      }
    } catch(err) {
      this.control = new FormControl();
    }

  }


  writeValue(formValue: string | []): void {
    // if(this.control) {
    //   if(!this.control.value) {

    //   }
    // }
    // this.control ? this.control.setValue(formValue) : (this.control = new FormControl(formValue));
    if(!this.control) {
      this.control = new FormControl(formValue);
    }
    this.selected = formValue;
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    console.log('Hello');
  }

  openDropdown() {
    this.isOpened = true;
  }
  toggleDropDown() {
    this.isOpened = !this.isOpened;
  }
  closeDropdown() {
    this.isOpened = false;
  }

  get classes() {
    const classes = [];
    classes.push(`select--${this.size}`);
    return classes;
  }
  select(index: number) {
    this.onChange(this.options[index]);
    this.selected = this.options[index];
    this.closeDropdown();
  }
}
