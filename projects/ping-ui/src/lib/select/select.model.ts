export interface OptionsModel {
    value: any;
    selected: boolean;
}

export type SelectSizes = 'sm' | 'md' | 'lg';