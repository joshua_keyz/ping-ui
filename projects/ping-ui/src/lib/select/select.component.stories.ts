import { Meta, StoryObj, componentWrapperDecorator, moduleMetadata } from "@storybook/angular";
import { PUISelectComponent } from "./select.component";
import { PLIconComponent } from "../icon/icon.component";
import { FormControl, FormGroup, ReactiveFormsModule } from "@angular/forms";
import { FormFieldComponent } from "../form-field/form-field.component";

export default {
    title: 'Components/Select',
    component: PUISelectComponent,
    tags: ['autodocs'],
    decorators: [
        moduleMetadata({
            declarations: [
                PLIconComponent,
                FormFieldComponent,
            ],
            imports: [
                ReactiveFormsModule
            ]
        }),
        componentWrapperDecorator((story) => `<div style="margin: 3em; width: 300px;">${story}</div>`)
    ],
    argTypes: {
        size: {
            control: 'select',
            options: ['sm', 'md', 'lg']
        }
    }
} as Meta<PUISelectComponent<string>>;

type Story = StoryObj<PUISelectComponent<string>>;

export const Default: Story = {
    args: {
        size: 'sm',
        options: [
            'Option 1',
            'Option 2',
            'Option 3',
            'Option 4'
        ],
        label: 'My Label'
    },
    render: (args) => ({
        props: {
            formGroup: new FormGroup({
                colors: new FormControl('Option 2'),
                name: new FormControl('Joshua'),
            }),
            ...args
        },
        template: `
            <form [formGroup]="formGroup">
                <pui-select [label]="label" [size]="size" formControlName="colors" [options]="options"></pui-select>
            </form>
        `
    }),
}