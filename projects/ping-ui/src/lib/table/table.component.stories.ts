import { Meta, StoryObj, componentWrapperDecorator, moduleMetadata } from "@storybook/angular";
import { PUITableComponent } from "./table.component";
import { PLIconComponent } from "../icon/icon.component";
import { PUISelectComponent } from "../select/select.component";
import { ReactiveFormsModule } from "@angular/forms";
import { PLButtonComponent } from "../button/button.component";
import { delay, of } from "rxjs";
import { PUISpinnerComponent } from "../spinner/spinner.component";
export default {
  title: 'Components/Data Table',
  component: PUITableComponent,
  tags: ['autodocs'],
  decorators: [
      moduleMetadata({
          declarations: [
              PUITableComponent,
              PLIconComponent,
              PUISelectComponent,
              PLButtonComponent,
              PUISpinnerComponent,
          ],
          imports: [
            ReactiveFormsModule,
          ]
      }),
      componentWrapperDecorator((story) => `<div style="margin: 3em; min-width: 300px;">${story}</div>`)
  ],
  argTypes: {

  }
} as Meta<PUITableComponent>;

type Story = StoryObj<PUITableComponent>;

export const Default: Story = {
  args: {
      data$: of([{
        '#': 1,
        first: 'Mark',
        last: 'Otto',
        handle: '@mdo',
        extra: 'extra'
      },{
        '#': 2,
        first: 'Jacob',
        last: 'Thornton',
        handle: '@fat',
        extra: 'extra'
      },
      {
        '#': 3,
        first: 'Larry',
        last: 'The Bird',
        handle: '@twitter',
        extra: 'extra'
      },
      {
        '#': 4,
        first: 'Jeff',
        last: 'Hardy',
        handle: '@hardy',
        extra: 'extra'
      },{
        '#': 5,
        first: 'Micheal',
        last: 'Ezra',
        handle: '@ezra',
        extra: 'extra'
      },{
        '#': 6,
        first: 'London',
        last: 'Bradley',
        handle: '@brad',
        extra: 'extra'
      },{
        '#': 7,
        first: 'Miracle',
        last: 'Cash',
        handle: '@mircash',
        extra: 'extra'
      },{
        '#': 8,
        first: 'Joshua',
        last: 'Wood',
        handle: '@wood',
        extra: 'extra'
      },{
        '#': 9,
        first: 'Blessing',
        last: 'Carey',
        handle: '@blessed',
        extra: 'extra'
      },{
        '#': 10,
        first: 'Monday',
        last: 'Benson',
        handle: '@monday',
        extra: 'extra'
      },{
        '#': 11,
        first: 'Lizzy',
        last: 'Markle',
        handle: '@markliz',
        extra: 'extra'
      },{
        '#': 12,
        first: 'Larry',
        last: 'Davidson',
        handle: '@larry',
        extra: 'extra'
      },{
        '#': 13,
        first: 'Frodo',
        last: 'Thomson',
        handle: '@frodo',
        extra: 'extra'
      },{
        '#': 14,
        first: 'Tristain',
        last: 'Lingy',
        handle: '@lingy',
        extra: 'extra'
      },{
        '#': 15,
        first: 'Jackson',
        last: 'Perry',
        handle: '@jackson_perry',
        extra: 'extra'
      },{
        '#': 16,
        first: 'Shonde',
        last: 'Pelumi',
        handle: '@shonde',
        extra: 'extra'
      },{
        '#': 17,
        first: 'Oghene',
        last: 'Micheal',
        handle: '@oghene_micheals',
        extra: 'extra'
      }
    ]).pipe(delay(3000)),
      qtySet: [5, 10, 15],
      columns: ['#', 'first', 'last', 'handle'],
  },
  render: (args) => ({
    props: {
      ...args,
    },
    template: `
      <pui-table
        [data$]="data$"
        [qtySet]="qtySet"
        [columns]="columns"
        [columnTemplate]="columnTemplate"
        [emptyTemplate]="emptyTemplate"
      >
      </pui-table>
      <ng-template #emptyTemplate>
        <div class="display-1r" [ngStyle]="{
          color: '#BDBDBD',
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center'
        }">
          <pui-icon [ngStyle]="{
            fontSize: '48px',
            color: '#BDBDBD'
          }" iconName="icon-web"></pui-icon>
          <p>No Data has been added yet</p>
          <p>Do something to add data</p>
        </div>
      </ng-template>
      <ng-template #columnTemplate let-column="column" let-row="row">
        <ng-container [ngSwitch]="column">
          <ng-container *ngSwitchCase="'handle'">
            <pui-button
              [variant]="'secondary'"
              type="outlined"
              [content]="row[column]"
            ></pui-button>
          </ng-container>
          <ng-container *ngSwitchCase="'#'">
            <div [ngStyle]="{
              display: 'flex',
            }">
            <div [ngStyle]="{
              width: '20px',
              height: '20px',
              borderRadius: '50%',
              marginRight: '5px',
              backgroundColor: 'red'
            }"></div>
            <span>{{row[column]}}</span>
            </div>
          </ng-container>
          <ng-container *ngSwitchDefault>
            {{row[column]}}
          </ng-container>
        </ng-container>
      </ng-template>
    `
  }),
}

export const EmptyTable: Story = {
  ...Default,
  args: {
    data$: of([]).pipe(delay(3000)),
    qtySet: [5, 10, 15],
    columns: ['#', 'first', 'last', 'handle'],
  }
}