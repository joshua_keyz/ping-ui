import { ChangeDetectorRef, Component, Input, OnDestroy, OnInit, TemplateRef } from "@angular/core";
import { Data } from "./data.interface";
import { FormControl } from "@angular/forms";
import { Observable, Subject, takeUntil } from "rxjs";

@Component({
  selector: 'pui-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class PUITableComponent implements OnInit, OnDestroy {
  @Input() data$: Observable<Data[]>;
  @Input() qtySet = [50, 100, 200];
  @Input() columns: string[];
  @Input() columnTemplate: TemplateRef<ColumnTemplateContext>;
  @Input() emptyTemplate: TemplateRef<any>;
  data: Data[];
  isLoading = false;
  totalItems = 0;
  currentPage = 0;
  displayedData: Data[] = [];
  pageStats = '';
  onDestroy$ = new Subject();
  qtySelector = new FormControl();
  constructor(private cd: ChangeDetectorRef) {}
  ngOnInit(): void {
    this.isLoading = true
    this.qtySelector.setValue(this.qtySet[0]);
    this.qtySelector.valueChanges.subscribe(() => {
      this.getDisplayedData()
    });

    this.data$.pipe(
      takeUntil(this.onDestroy$),
    ).subscribe(
      data => {
        this.data = data;
        this.totalItems = data.length;
        this.getDisplayedData();
        this.isLoading = false;
      }
    );
    
    
  }
  getColumnHeadings() {
    return Object.keys(this.data[0]);
  }

  getColumnValue(obj: Data) {
    return Object.values(obj); 
  }

  getLastPage() {
    const numberOfFullPages = this.data.length / this.itemsPerPage;
    const hasRemnant = this.data.length % this.itemsPerPage;
    if(hasRemnant) {
      return Math.floor(numberOfFullPages) + 1;
    }
    return numberOfFullPages;
  }
  
  get itemsPerPage() {
    return +this.qtySelector.value;
  }
  getPageStats(): string{
    if(this.currentPage === 0) {
      if(this.itemsPerPage > this.data.length) {
        return `1 - ${this.data.length} of ${this.data.length}`
      }
      return `1 - ${this.itemsPerPage} of ${this.data.length}`
    } else if(this.currentPage === this.getLastPage() - 1){
      return `${(this.itemsPerPage * this.currentPage) + 1} - ${(this.data.length)} of ${this.data.length}`
    }
    else{
      return `${(this.itemsPerPage * this.currentPage) + 1} - ${(this.itemsPerPage * this.currentPage) + this.itemsPerPage} of ${this.data.length}`
    }
  }
  ngOnDestroy(): void {
    this.onDestroy$.complete();
  }
  getDisplayedData(): void {
    if(this.currentPage === 0) {
      this.displayedData = this.data.slice(0, this.itemsPerPage);
    } else {
      const nextItemStart = this.data.slice(this.itemsPerPage * this.currentPage, this.itemsPerPage * (this.currentPage + 1))
      this.displayedData =  nextItemStart;
    }
    this.pageStats = this.getPageStats();
  }
  goToNextPage() {
    if(this.currentPage < this.getLastPage() -1) {
      this.currentPage++;
      this.getDisplayedData();
    }
  }
  goToPrevPage() {
    if(this.currentPage !== 0) {
      this.currentPage--;
      this.getDisplayedData();
    }
  }
  goToFirstPage() {
    this.currentPage = 0;
    this.getDisplayedData();
  }

  goToLastPage() {
    this.currentPage = this.getLastPage() - 1;
    this.getDisplayedData();
  }
}

export interface ColumnTemplateContext {
  row: Data;
  column: string;
}