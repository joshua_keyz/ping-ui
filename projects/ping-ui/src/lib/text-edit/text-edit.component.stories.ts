import { Meta, StoryObj, componentWrapperDecorator, moduleMetadata } from "@storybook/angular";
import { PUITextEditComponent } from "./text-edit.component";
import { FormFieldComponent } from "../form-field/form-field.component";
import { PLButtonComponent } from "../button/button.component";
import { PLIconComponent } from "../icon/icon.component";

export default {
  title: 'Components/Text Edit',
  component: PUITextEditComponent,
  tags: ['autodocs'],
  decorators: [
    moduleMetadata({
      declarations: [
        PUITextEditComponent,
        FormFieldComponent,
        PLButtonComponent,
        PLIconComponent,
      ]
    }),
    componentWrapperDecorator(story => `<div style="width: 500px; margin: 50px;">${story}</div>`)
  ],
  argTypes: {
    size: {
      control: 'select',
      options: ['sm', 'md', 'lg'],
    },
    type: {
      control: 'select',
      options: ['text', 'password', 'number', 'email'],
    },
  }
} as Meta<PUITextEditComponent>;

type Story = StoryObj<PUITextEditComponent>;

export const EmailEdit: Story = {
  args: {
    value: 'joshua.oguma@outlook.com',
    label: 'Email',
    size: 'md',
    type: 'email'
  }
}
export const PasswordEdit: Story = {
  args: {
    value: 'mypassword',
    label: 'Email',
    size: 'md',
    type: 'password'
  }
}



