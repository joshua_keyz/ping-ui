import { Component, EventEmitter, Input, Output } from "@angular/core";
import { ButtonSize } from "../button/button.model";

@Component({
  selector: 'pui-text-edit',
  templateUrl: './text-edit.component.html',
  styleUrls: ['./text-edit.component.scss']
})
export class PUITextEditComponent {
  @Input() size: ButtonSize = 'sm';
  @Input() label: string;
  @Input() id: string;
  @Input() value: string;
  @Input() type: string;
  @Output() edit = new EventEmitter<string>();

  get getClasses() {
    const classes = [];
    switch (this.size) {
      case 'sm':
        classes.push('text-edit--sm');
        break;
      case 'lg':
        classes.push('text-edit--lg');
        break;
      case 'md':
        classes.push('text-edit--md');
        break;
    }
    return classes;
  }
}