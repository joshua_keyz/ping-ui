import { Meta, StoryObj, componentWrapperDecorator, moduleMetadata } from "@storybook/angular";
import { PUIImageCropperComponent } from "./img-cropper.component";
import { ReactiveFormsModule } from "@angular/forms";
import { PLButtonComponent } from "../button/button.component";


export default {
  title: 'Components/Image Cropper',
  component: PUIImageCropperComponent,
  tags: ['autodocs'],
  decorators: [
      moduleMetadata({
          declarations: [
             PLButtonComponent,
          ],
          imports: [
            ReactiveFormsModule,
          ]
      }),
      componentWrapperDecorator((story) => `<div style="margin: 3em; width:300px">${story}</div>`)
  ],
  argTypes: {

  }
} as Meta<PUIImageCropperComponent>;

type Story = StoryObj<PUIImageCropperComponent>;

export const Default: Story = {
  args: {
    initialImage: 'https://images.unsplash.com/photo-1693230162452-f77d79563f3d?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=991&q=80'
  }
}