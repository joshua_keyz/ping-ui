import { AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, Output, ViewChild } from "@angular/core";
import Cropper from 'cropperjs';
@Component({
  selector: 'pui-img-cropper',
  templateUrl: './img-cropper.component.html',
  styleUrls: ['./img-cropper.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PUIImageCropperComponent implements AfterViewInit {
  @Input() initialImage: any;
  @Output() done = new EventEmitter<string>();
  previewImageSrc: any;
  cropper: Cropper;
  view : 'edit' | 'preview' = 'edit';
  @ViewChild('image') image: any;
  @ViewChild('previewImage') previewImage: any;
  completedSrc: any;

  constructor(private cdRef: ChangeDetectorRef){}

  ngAfterViewInit(): void {
    this.image.nativeElement.setAttribute('src', this.initialImage);

    if(this.cropper) {
      this.cropper.destroy();
    }
    setTimeout((function() {
      this.cropper = new Cropper(this.image.nativeElement, {checkOrientation: false});
      this.cropper.setAspectRatio(1)
      this.cdRef.detectChanges();
    }).bind(this), 0)
  }
  openFilePanel(e: MouseEvent) {
    e.preventDefault()
  } 
  initNew(e) {
    const value = e.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(value)
    reader.onload = () => {
      this.image.nativeElement.setAttribute('src', reader.result);

      if(this.cropper) {
        this.cropper.destroy();
      }
      this.cropper = new Cropper(this.image.nativeElement, {checkOrientation: false});
      this.cropper.setAspectRatio(1)
    }
  }
  showPreview(e) {
    e.preventDefault();
    console.log(this.cropper.getCroppedCanvas({}).toDataURL())
    this.view = 'preview';
    console.log(this.cropper)
    const imgSrc = this.cropper.getCroppedCanvas({}).toDataURL();
    this.previewImage.nativeElement.src = imgSrc;
  }
  showEdit() {
    this.view = 'edit';
  } 
  upload() {
    this.done.emit(this.previewImage.nativeElement.src);
  }
}