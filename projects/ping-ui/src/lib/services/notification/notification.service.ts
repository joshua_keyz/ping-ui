import { Injectable } from "@angular/core";
import { Subject } from "rxjs";
import { NotificationModel } from "../../models/notification.model";

@Injectable()
export class NotificationService {
  private notificationSource = new Subject<NotificationModel>();

  notification$ = this.notificationSource.asObservable();

  sendNotification(data: NotificationModel) {
    this.notificationSource.next(data);
  }
}