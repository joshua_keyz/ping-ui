import {
  Component,
  Host,
  Input,
  OnChanges,
  OnInit,
  Optional,
  SimpleChanges,
  SkipSelf,
} from '@angular/core';
import {
  AbstractControl,
  ControlContainer,
  ControlValueAccessor,
  NG_VALUE_ACCESSOR,
} from '@angular/forms';

/**
 * This component is used for adding form fields to a page.
 */
@Component({
  selector: 'pui-form-field',
  templateUrl: './form-field.component.html',
  styleUrls: ['./form-field.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: FormFieldComponent,
      multi: true,
    },
  ],
})
export class FormFieldComponent
  implements ControlValueAccessor, OnChanges, OnInit
{
  @Input() size: 'sm' | 'md' | 'lg' = 'sm';
  @Input() label: string;
  @Input() type: string;
  @Input() id: string;
  @Input() formControlName: string;
  @Input() placeholder: string;

  public control: AbstractControl;

  constructor(
    @Optional() @Host() @SkipSelf() private controlContainer: ControlContainer
  ) {}

  value: string;
  onChange: (value: any) => any;
  onTouched: (value: any) => any;

  ngOnChanges(changes: SimpleChanges) {
    console.log(changes);
  }
  ngOnInit(): void {
    if (this.controlContainer) {
      if (this.formControlName) {
        this.control = this.controlContainer.control.get(this.formControlName);
        console.log(this.control, 'yay');
      } else {
        console.warn(
          'Missing FormControlName directive from host element of the component'
        );
      }
    } else {
      console.warn(`Can't find parent FormGroup directive'`);
    }
  }

  processChange(event) {
    this.onChange(event.target.value);
  }

  get getClasses() {
    const classes = [];
    switch (this.size) {
      case 'sm':
        classes.push('form-field--sm');
        break;
      case 'lg':
        classes.push('form-field--lg');
        break;
      case 'md':
        classes.push('form-field--md');
        break;
    }

    if (this.control?.invalid && this.control?.dirty) {
      classes.push('form-field--invalid');
    }
    return classes;
  }

  writeValue(value: any): void {
    this.value = value;
  }
  registerOnChange(onChange: any): void {
    this.onChange = onChange;
  }
  registerOnTouched(onTouched: any): void {
    this.onTouched = onTouched;
  }
}
