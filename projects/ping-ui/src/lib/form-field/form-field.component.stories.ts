import {
  Meta,
  StoryObj,
  componentWrapperDecorator,
  moduleMetadata,
} from '@storybook/angular';
import { FormFieldComponent } from './form-field.component';
import {
  FormControl,
  FormGroup,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';

const meta: Meta<FormFieldComponent> = {
  title: 'Components/Form Field',
  component: FormFieldComponent,
  tags: ['autodocs'],
  decorators: [
    moduleMetadata({
      declarations: [],
      imports: [ReactiveFormsModule],
    }),
    componentWrapperDecorator(
      (story) => `<div style="margin: 3em; max-width: 300px">${story}</div>`
    ),
  ],
  argTypes: {
    size: {
      control: 'select',
      options: ['sm', 'md', 'lg'],
    },
    type: {
      control: 'select',
      options: ['text', 'password', 'number'],
    },
  },
  args: {
    type: 'text',
  },
};
export default meta;

type Story = StoryObj<FormFieldComponent>;

export const Default: Story = {
  args: {
    size: 'sm',
    label: 'Label',
    id: 'id1',
    formControlName: '',
    placeholder: 'Enter a value'
  },
  render: (args) => ({
    props: {
      formGroup: new FormGroup({
        firstName: new FormControl('Text Item', Validators.required),
      }),
      ...args,
    },
    template: `
        <form [formGroup]="formGroup">
        <pui-form-field [placeholder]="placeholder" formControlName="firstName" [id]="id" [type]="type" [size]="size" [label]="label">
            <div errorMessage>
                The field is required
            </div>
        </pui-form-field>
        </form>`,
  }),
};

export const WithErrorMessage: Story = {
  args: {
    ...Default.args,
    id: 'id2',
  },
  render: (args) => ({
    props: {
      formGroup: new FormGroup({
        firstName: new FormControl('', Validators.required),
      }),
      ...args,
    },
    template: `<form [formGroup]="formGroup">
    <pui-form-field  [placeholder]="placeholder" formControlName="firstName" [id]="id" [type]="type" [size]="size" [label]="label">
        <div errorMessage>
            The field is required
        </div>
    </pui-form-field>
    </form>`,
  }),
};

export const WithPasswordText: Story = {
  args: {
    ...Default.args,
    type: 'password',
    id: 'id3',
  },
  render: (args) => ({
    props: {
      formGroup: new FormGroup({
        firstName: new FormControl('Text Item', Validators.required),
      }),
      ...args,
    },
    template: `<form [formGroup]="formGroup">
    <pui-form-field  formControlName="firstName" [type]="type" [id]="id" [size]="size" [label]="label">
        <div errorMessage>
            The field is required
        </div>
    </pui-form-field>
    </form>`,
  }),
};
