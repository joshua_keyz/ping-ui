import { Meta, StoryObj, componentWrapperDecorator, moduleMetadata } from '@storybook/angular';
import { PLIconComponent } from './icon.component';

const meta: Meta<PLIconComponent> = {
    title: 'Components/Icon',
    component: PLIconComponent,
    tags: ['autodocs'],
    decorators: [
        moduleMetadata({
            declarations: []
        }),
        componentWrapperDecorator((story) => `<div style="margin: 3em">${story}</div>`)
    ],
};

export default meta;

type IconStory = StoryObj<PLIconComponent>

export const Default: IconStory = {
    args: {
        iconName: 'icon-close'
    }
};