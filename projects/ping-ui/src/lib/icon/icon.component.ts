import { Component, Input } from "@angular/core";

@Component({
    selector: 'pui-icon',
    templateUrl: './icon.component.html',
    styleUrls: ['./icon.component.scss']
})
export class PLIconComponent {
    @Input() iconName = "icon-Assignment-turned-in"
}