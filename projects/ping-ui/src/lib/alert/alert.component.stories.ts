import { Meta, StoryObj, componentWrapperDecorator, moduleMetadata } from '@storybook/angular';
import { PLAlertComponent } from './alert.component';
import { PLIconComponent } from '../icon/icon.component';
import { PLButtonComponent } from '../button/button.component';
import { action } from '@storybook/addon-actions';

const meta: Meta<PLAlertComponent> = {
    title: 'Components/Alert',
    component: PLAlertComponent,
    decorators: [
        moduleMetadata({
            declarations: [
                PLIconComponent,
                PLButtonComponent,
            ]
        }),
        componentWrapperDecorator((story) => `<div style="margin: 3em">${story}</div>`)
    ],
    argTypes: {
        variant: {
            control: 'select',
            options: ['primary', 'secondary', 'danger']
        }
    }
}

export default meta;

type Story = StoryObj<PLAlertComponent>;

export const DefaultAlert: Story = {
    args: {
        variant: 'primary',
        closable: false,
    },
    render: (args) => ({
        props: {...args, cancel: action('click')},
        template: `
            <pui-alert [variant]="variant" [closable]="closable" (alertClose)="cancel('This alert was cancelled')">This is an alert</pui-alert>
        `
    })
}
export const ClosableAlert: Story = {
    args: {
        variant: 'primary',
        closable: true,
    },
    render: (args) => ({
        props: {...args, cancel: action('click')},
        template: `
            <pui-alert [variant]="variant" [closable]="closable" (alertClose)="cancel('This alert was cancelled')">This is an alert</pui-alert>
        `
    })
}