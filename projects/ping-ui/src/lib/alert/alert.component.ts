import { Component, EventEmitter, Input, Output } from "@angular/core";
import { AlertVariants } from "./alert.model";

@Component({
    selector: 'pui-alert',
    templateUrl: './alert.component.html',
    styleUrls: ['./alert.component.scss']
})
export class PLAlertComponent {
    @Input() variant: AlertVariants;
    @Input() closable: boolean;
    @Output() alertClose = new EventEmitter<void>();

    get classes() {
        console.log(this.variant);
        const alertVariant = `alert-${this.variant}`
        return [alertVariant];
    }
    closeAlert() {
        this.alertClose.emit();
    }
}