import { Meta, StoryObj, componentWrapperDecorator, moduleMetadata } from '@storybook/angular';
import { PUIProfileComponent } from './profile.component';
import { PLIconComponent } from '../icon/icon.component';

const meta: Meta<PUIProfileComponent> = {
    title: 'Components/User Profile',
    component: PUIProfileComponent,
    tags: ['autodocs'],
    decorators: [
        moduleMetadata({
            declarations: [
              PLIconComponent,
            ]
        }),
        componentWrapperDecorator((story) => `<div style="margin: 3em; width: 285px">${story}</div>`)
    ],
};

export default meta;

type ProfileStory = StoryObj<PUIProfileComponent>

export const Default: ProfileStory = {
  args: {
    // profilePic: 'https://images.unsplash.com/photo-1689613303199-90769f975895?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=715&q=80',
    profileName: 'Benita Fayokun',
    menuItems: [
      {
        icon: 'icon-account-box',
        item: 'My Account',
        link: 'route3',
      },
      {
        icon: 'icon-notification-bell',
        item: 'Notifications',
        link: 'route2',
      },
    ]
  }
};