import { Component, Input, OnInit } from '@angular/core';
import { MenuItemModel } from '../models/menu-item.model';

@Component({
  selector: 'pui-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
})
export class PUIProfileComponent implements OnInit {
  @Input() profilePic =
    'https://pinglink-space.s3.eu-north-1.amazonaws.com/pinglink-assets/profile-placeholder.png';
  @Input() profileName: string;
  @Input() menuItems: MenuItemModel[];
  @Input() subscription = 'freemium';

  menuOpened = false;

  ngOnInit(): void {
    console.log(this.profileName, this.menuItems, this.profilePic);
  }
  closeMenu() {
    alert('Hello');
  }
  toggleMenu() {
    this.menuOpened = !this.menuOpened;
  }
}
