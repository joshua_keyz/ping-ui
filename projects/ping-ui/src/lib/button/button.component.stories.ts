import { Meta, StoryObj, componentWrapperDecorator, moduleMetadata } from '@storybook/angular';
import { PLButtonComponent } from './button.component';
import { PLIconComponent } from '../icon/icon.component';

const meta: Meta<PLButtonComponent> = {
    title: 'Components/Button',
    component: PLButtonComponent,
    tags: ['autodocs'],
    decorators: [
        moduleMetadata({
            declarations: [
                PLIconComponent,
            ]
        }),
        componentWrapperDecorator((story) => `<div style="margin: 3em">${story}</div>`)
    ],
    argTypes: {
        variant: {
            control: 'select',
            options: ['primary', 'secondary', 'danger', 'success', 'warning', 'info'],
        },
        size: {
            control: 'select',
            options: ['sm', 'md', 'lg']
        },
        type: {
            control: 'select',
            options: ['filled', 'outlined', 'text', 'icon']
        }
    },
    args: {
        prefixIcon: 'icon-web',
        suffixIcon: 'icon-delete',
        disabled: false
    }
};

export default meta;

type ButtonStory = StoryObj<PLButtonComponent>

export const Filled: ButtonStory = {
    args: {
        content: "Button",
        variant: 'primary',
        size: 'sm',
        type: 'filled',
    },
    render: (args) => ({
        props: args,
        template: `
        <pui-button
            [type]="type"
            [variant]="variant"
            [disabled]="disabled"
            [size]="size"
            [content]="content"
            [prefixIcon]="prefixIcon"
            [suffixIcon]="suffixIcon"
        ></pui-button>`
    })
};
export const Outlined: ButtonStory = {
    args: {
        content: "Button",
        variant: 'primary',
        size: 'sm',
        type: 'outlined',
    },
    render: (args) => ({
        props: args,
        render: (args) => ({
            props: args,
            template: `
            <pui-button
                [type]="type"
                [variant]="variant"
                [size]="size"
                [content]="content"
                [prefixIcon]="prefixIcon"
                [disabled]="disabled"
                [suffixIcon]="suffixIcon"
            ></pui-button>`
        })
    })
};
export const Text: ButtonStory = {
    args: {
        content: "Button",
        variant: 'primary',
        size: 'sm',
        type: 'text'
    },
    render: (args) => ({
        props: args,
        render: (args) => ({
            props: args,
            template: `
            <pui-button
                [type]="type"
                [variant]="variant"
                [size]="size"
                [disabled]="disabled"
                [content]="content"
                [prefixIcon]="prefixIcon"
                [suffixIcon]="suffixIcon"
            ></pui-button>`
        })
    })
};
export const Icon: ButtonStory = {
    args: {
        content: "Button",
        variant: 'primary',
        size: 'sm',
        type: 'icon',
        icon: 'icon-accessibility'
    },
    render: (args) => ({
        props: args,
        render: (args) => ({
            props: args,
            template: `
            <pui-button
                [type]="type"
                [variant]="variant"
                [icon]="icon"
                [size]="size"
                [disabled]="disabled"
                [content]="content"
                [prefixIcon]="prefixIcon"
                [suffixIcon]="suffixIcon"
            ></pui-button>`
        })
    })
};