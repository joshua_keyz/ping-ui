export type ButtonVariant = 'primary' | 'secondary' | 'danger';
export type ButtonType = 'filled' | 'outlined' | 'text' | 'icon';
export type ButtonSize = 'sm' | 'md' | 'lg';