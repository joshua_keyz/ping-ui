import { Component, Input } from "@angular/core";
import { ButtonSize, ButtonType, ButtonVariant } from "./button.model";

@Component({
    selector: 'pui-button',
    templateUrl: './button.component.html',
    styleUrls: ['./button.component.scss']
})
export class PLButtonComponent {
    @Input() content: string;
    @Input() type: ButtonType = 'filled';
    @Input() variant: ButtonVariant;
    @Input() size: ButtonSize = 'sm';
    @Input() suffixIcon: string;
    @Input() prefixIcon: string;
    @Input() disabled: boolean;
    @Input() icon: string;

    get classes() {
        const classes = [];
        const suffix = 'btn-';
        classes.push(`${suffix}${this.type}-${this.variant}`);
        classes.push(suffix + this.size);
        return classes;
    }
    
}