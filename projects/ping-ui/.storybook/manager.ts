import { addons } from '@storybook/manager-api';
import pingUiTheme from './pinglink-theme';

addons.setConfig({
    theme: pingUiTheme,

})