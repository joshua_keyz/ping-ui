import type { StorybookConfig } from "@storybook/angular";

const config: StorybookConfig = {

  // Required
  stories: ["../src/**/*.mdx", "../src/**/*.stories.ts"],
  framework: {
    name: "@storybook/angular",
    options: {},
  },
  staticDirs: ['../src/lib/public'],
  // Optional
  addons: [
    "@storybook/addon-links",
    "@storybook/addon-essentials",
    "@storybook/addon-knobs",
    "@storybook/addon-interactions",
  ],
  typescript: {
    check: false,
    checkOptions: {}
  },
  core: {
    disableTelemetry: true,
  },
  docs: {
    autodocs: 'tag',
  },
  features: {storyStoreV7: true},
  refs: {},
  logLevel: 'debug',
};
export default config;
