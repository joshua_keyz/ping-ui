import { create } from '@storybook/theming/create';

export default create({
    base: 'light',
    brandImage: 'https://pinglink-space.s3.eu-north-1.amazonaws.com/pinglink-assets/logo.svg',
    brandTitle: 'Ping-UI'
});
