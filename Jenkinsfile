pipeline {
    agent any
    environment {
        GIT_AUTHOR_NAME = 'Joshua Avwerosuoghene Oguma'
        GIT_AUTHOR_EMAIL = 'joshua.oguma@outlook.com'
        NPM_TOKEN = credentials('NPM_TOKEN')
        GL_TOKEN = credentials('GL_TOKEN')
    }

    options {
        gitLabConnection('pingui')
    }
    
    stages {
        stage('Install Deps') {
            steps {
                nodejs(nodeJSInstallationName: 'nodejs') {
                    sh 'npm install'
                }
                script {
                    updateGitlabCommitStatus name: 'jenkins-build', state: 'running'
                }
            }
        }

        // stage('lint') {
        //     steps {
        //         nodejs(nodeJSInstallationName: 'nodejs') {
        //             sh 'npm run lint'
        //         }
        //     }
        // }

        stage('Build') {
            steps {
                nodejs(nodeJSInstallationName: 'nodejs') {
                    sh 'npm run build:ping-ui'
                    sh 'npm run build-storybook'
                }
            }
        }
        stage('Publish') {
            when {
                anyOf {
                    branch 'master'
                    branch 'beta'
                }
            }
            stages {
                stage('Publish NPM Library') {
                    steps {
                        nodejs(nodeJSInstallationName: 'nodejs') {
                            sh 'npm run semantic-release'
                        }
                    }
                }
                stage('Deploy Storybook') {
                    steps{
                        nodejs(nodeJSInstallationName: 'nodejs') {
                            script {
                                SERVER = 'pinglink-storybook.joshkeys.site';
                                TMP_DEST = '/app/tmp/ping-ui'
                                DEST = '/app/ping-ui'
                                FUNCTIONAL_USER = 'dsp_adm'

                                env.LATEST_VERSION = sh(script: "git describe --tags --abbrev=0", returnStdout: true).trim()
                            }
                            withCredentials([usernamePassword(credentialsId: 'pinglink-deployer', passwordVariable: 'PASSWORD', usernameVariable: 'USERNAME')]) {
                                sh "sshpass -p '${PASSWORD}' ssh -o StrictHostKeyChecking=no ${USERNAME}@${SERVER} sudo rm -rf ${TMP_DEST}"
                                sh "sshpass -p '${PASSWORD}' scp -o StrictHostKeyChecking=no  -r ./storybook-static ${USERNAME}@${SERVER}:${TMP_DEST}"
                                sh "sshpass -p '${PASSWORD}' scp -o StrictHostKeyChecking=no scripts/deploy_docs.sh ${USERNAME}@${SERVER}:/tmp/deploy_doc_${BUILD_NUMBER}.sh"
                                sh "sshpass -p '${PASSWORD}' ssh -o StrictHostKeyChecking=no ${USERNAME}@${SERVER} sudo chmod +x /tmp/deploy_doc_${BUILD_NUMBER}.sh"
                                sh "sshpass -p '${PASSWORD}' ssh -o StrictHostKeyChecking=no ${USERNAME}@${SERVER} sudo /tmp/deploy_doc_${BUILD_NUMBER}.sh ${TMP_DEST} ${DEST} ${env.LATEST_VERSION} ${FUNCTIONAL_USER}"
                                sh "sshpass -p '${PASSWORD}' ssh -o StrictHostKeyChecking=no ${USERNAME}@${SERVER} sudo rm /tmp/deploy_doc_${BUILD_NUMBER}.sh"
                            }
                        }

                        // ansiblePlaybook(
                        //     playbook: '/var/jenkins_home/ansible/play.yml',
                        //     inventory: '/var/jenkins_home/ansible/hosts',
                        //     credentialsId: 'pinglink-file',
                        //     colorized: true
                        // )
                    }
                }
            }
        }
        
    }

    post {
        success {
            script {
                updateGitlabCommitStatus name: 'jenkins-build', state: 'success'
            }
        }
        aborted {
            script {
                updateGitlabCommitStatus name: 'jenkins-build', state: 'canceled'
            }
        }
        failure {
            script {
                updateGitlabCommitStatus name: 'jenkins-build', state: 'failed'
            }
        }
    }
}
