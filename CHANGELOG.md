## [1.24.6](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.24.5...v1.24.6) (2025-03-15)


### Bug Fixes

* **logo:** updated logo ([4da74f0](https://gitlab.com/joshua_keyz/ping-ui/commit/4da74f020252d64c2a3f7bc8271cbdf01da06639))
* **profile:** updated logo paths ([6ef3a15](https://gitlab.com/joshua_keyz/ping-ui/commit/6ef3a15cf45029ac572585e5a3b8fc301688f1ba))

## [1.24.5](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.24.4...v1.24.5) (2025-01-25)


### Bug Fixes

* fixed broken image links ([e231b7d](https://gitlab.com/joshua_keyz/ping-ui/commit/e231b7dfd9f8bbb8a1fe3645a773680b78e292bf))

## [1.24.4](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.24.3...v1.24.4) (2025-01-07)


### Bug Fixes

* fixed broken deploy pipeline ([17cdae4](https://gitlab.com/joshua_keyz/ping-ui/commit/17cdae4eaa1ca96d42dbe0380c71297336c8bfa9))

## [1.24.3](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.24.2...v1.24.3) (2025-01-06)


### Bug Fixes

* fixed broken linting ([cf09383](https://gitlab.com/joshua_keyz/ping-ui/commit/cf093832506a82a449a4592d6951770ed9ba2459))
* fixed broken versions ([c767f08](https://gitlab.com/joshua_keyz/ping-ui/commit/c767f08223a351aec46c262e769bd711edbadea4))
* fixing ci/cd ([87cb150](https://gitlab.com/joshua_keyz/ping-ui/commit/87cb150b9628285eda28e33daf392393a90f9a5b))
* fixing ci/cd ([e6712aa](https://gitlab.com/joshua_keyz/ping-ui/commit/e6712aa5db40320e77961416541a867ffe65809f))

## [1.24.2](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.24.1...v1.24.2) (2023-10-13)


### Bug Fixes

* **profile:** fixed profile component to have a default subscription ([e479353](https://gitlab.com/joshua_keyz/ping-ui/commit/e4793539f114ccd6479c297c0e3330268b3041c8))

## [1.24.1](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.24.0...v1.24.1) (2023-10-13)


### Bug Fixes

* **layout:** fixed the layout component to receive subscription input and also profile component to also receive the subscription from the layout component ([91b894a](https://gitlab.com/joshua_keyz/ping-ui/commit/91b894aa43fac33c5dceedc128a9527eb763595a))

# [1.24.0](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.23.20...v1.24.0) (2023-09-27)


### Features

* **text-edit:** added edit text ([b775b82](https://gitlab.com/joshua_keyz/ping-ui/commit/b775b8237b6c01f85cc3dfc1114ccd0bc6809a3c))

## [1.23.20](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.23.19...v1.23.20) (2023-09-18)


### Bug Fixes

* fixed cropping ([c282dd6](https://gitlab.com/joshua_keyz/ping-ui/commit/c282dd68e8d25606afa6baf832371771d95c1edc))

## [1.23.19](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.23.18...v1.23.19) (2023-09-18)


### Bug Fixes

* added fix for cors issues ([c5b055b](https://gitlab.com/joshua_keyz/ping-ui/commit/c5b055b41cc3db3f7dcd12ca33e55602e31ed212))

## [1.23.18](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.23.17...v1.23.18) (2023-09-15)


### Bug Fixes

* fixing cropper ([a9f9647](https://gitlab.com/joshua_keyz/ping-ui/commit/a9f96474fa6e76b60e8191af338b0354407c8d9d))

## [1.23.17](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.23.16...v1.23.17) (2023-09-15)


### Bug Fixes

* fixed cross origin issues ([d59a340](https://gitlab.com/joshua_keyz/ping-ui/commit/d59a34082d0e7c45f5c0c15504317072110d9ae8))

## [1.23.16](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.23.15...v1.23.16) (2023-09-15)


### Bug Fixes

* fixing image cropper ([ca0f893](https://gitlab.com/joshua_keyz/ping-ui/commit/ca0f89322a0e379b86d708f33063779c88c41d6b))

## [1.23.15](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.23.14...v1.23.15) (2023-09-15)


### Bug Fixes

* fixing image cropper ([d8ef9bb](https://gitlab.com/joshua_keyz/ping-ui/commit/d8ef9bb7153467d0fdceea8973feffa8f2771604))

## [1.23.14](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.23.13...v1.23.14) (2023-09-15)


### Bug Fixes

* added some changes ([33fda94](https://gitlab.com/joshua_keyz/ping-ui/commit/33fda941154e74bd535c39a91a0c252bfa7b3884))

## [1.23.13](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.23.12...v1.23.13) (2023-09-15)


### Bug Fixes

* added changes ([c6f20ae](https://gitlab.com/joshua_keyz/ping-ui/commit/c6f20ae56a44bf09183f4eba70837e4d28970762))

## [1.23.12](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.23.11...v1.23.12) (2023-09-15)


### Bug Fixes

* fixing image cropper ([0cd877d](https://gitlab.com/joshua_keyz/ping-ui/commit/0cd877dc7e922e43261f0882b5e13c03f9354d4f))

## [1.23.11](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.23.10...v1.23.11) (2023-09-14)


### Bug Fixes

* fixed uploader component ([c34f2cc](https://gitlab.com/joshua_keyz/ping-ui/commit/c34f2cc349eaa22479ef45207874b45249187598))

## [1.23.10](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.23.9...v1.23.10) (2023-09-14)


### Bug Fixes

* fixing cropper.js ([63ad1c9](https://gitlab.com/joshua_keyz/ping-ui/commit/63ad1c9d21970b45ed32595c8a72496919656411))

## [1.23.9](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.23.8...v1.23.9) (2023-09-14)


### Bug Fixes

* fixed cropper ([f0edf1d](https://gitlab.com/joshua_keyz/ping-ui/commit/f0edf1d297cf8db0c267076be7e9c90e8dc21719))

## [1.23.8](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.23.7...v1.23.8) (2023-09-14)


### Bug Fixes

* fixed cropper.min.css ([4c72f9a](https://gitlab.com/joshua_keyz/ping-ui/commit/4c72f9a33571abd032da5671a6fe4075199662cc))

## [1.23.7](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.23.6...v1.23.7) (2023-09-14)


### Bug Fixes

* fixed styling ([0cc0571](https://gitlab.com/joshua_keyz/ping-ui/commit/0cc0571da6fcc29896e88eab3652611ac96d3306))

## [1.23.6](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.23.5...v1.23.6) (2023-09-14)


### Bug Fixes

* fixed styling issues ([5827fec](https://gitlab.com/joshua_keyz/ping-ui/commit/5827fec1b4a0748b70894488bce1fe4740de2fe1))

## [1.23.5](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.23.4...v1.23.5) (2023-09-14)


### Bug Fixes

* fixing build ([1417248](https://gitlab.com/joshua_keyz/ping-ui/commit/14172484adc19035cc6de4ff960934e12f13f803))
* fixing styling issues ([54f072f](https://gitlab.com/joshua_keyz/ping-ui/commit/54f072f3bef0c1af0c928b9cbe9e667ae6c044a6))

## [1.23.4](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.23.3...v1.23.4) (2023-09-08)


### Bug Fixes

* added peer dependencies ([763756e](https://gitlab.com/joshua_keyz/ping-ui/commit/763756e7fa833169a2e1d1b170575cf8baba1795))

## [1.23.3](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.23.2...v1.23.3) (2023-09-08)


### Bug Fixes

* fixing cropperjs ([5b972cf](https://gitlab.com/joshua_keyz/ping-ui/commit/5b972cf5b708f8f2eb59be818f7b5ba8ae867753))

## [1.23.2](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.23.1...v1.23.2) (2023-09-08)


### Bug Fixes

* fixing cropperjs ([68d50a7](https://gitlab.com/joshua_keyz/ping-ui/commit/68d50a70f89541eb6f99bc67d6b1ce509d360019))
* fixing cropperjs ([1195ce6](https://gitlab.com/joshua_keyz/ping-ui/commit/1195ce6fead69d4f7e916cccbb8b22360f6e27ad))

## [1.23.1](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.23.0...v1.23.1) (2023-09-08)


### Bug Fixes

* **img-cropper:** fixing cropper imports ([e692400](https://gitlab.com/joshua_keyz/ping-ui/commit/e692400ce6b942c6a67275166c944bb3d8acbe06))
* **img-cropper:** fixing cropper imports ([316d9e8](https://gitlab.com/joshua_keyz/ping-ui/commit/316d9e800b52408062f6ce1f67991d67b853be4b))

# [1.23.0](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.22.8...v1.23.0) (2023-09-08)


### Bug Fixes

* **image-cropper:** removed console.logs ([3869b27](https://gitlab.com/joshua_keyz/ping-ui/commit/3869b275c16b6585cf288f3c40d975217b8d9849))


### Features

* **image-cropper:** implemented image cropper component ([b035ad8](https://gitlab.com/joshua_keyz/ping-ui/commit/b035ad8d9edc4dec6b6d9ad505bd2d737b34e6f5))

## [1.22.8](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.22.7...v1.22.8) (2023-09-05)


### Bug Fixes

* **layout:** fixed the rendition of the layout ([a2881d7](https://gitlab.com/joshua_keyz/ping-ui/commit/a2881d708c241d2be22d7b74048063283d9bfbfd))

## [1.22.7](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.22.6...v1.22.7) (2023-09-02)


### Bug Fixes

* **table:** fixed styling ([298f264](https://gitlab.com/joshua_keyz/ping-ui/commit/298f2640ad8e3113d41460a66e9b9d4ec548f93f))

## [1.22.6](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.22.5...v1.22.6) (2023-09-02)


### Bug Fixes

* **table:** fixed table styling ([d0736d5](https://gitlab.com/joshua_keyz/ping-ui/commit/d0736d5e6bf31c5b9e8a6b316d5a28c7fa0f0256))
* **table:** fixed table styling ([d5cb2d8](https://gitlab.com/joshua_keyz/ping-ui/commit/d5cb2d8e2d119fc0bf379c3d1a9cbbe2bbb3d9e5))

## [1.22.5](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.22.4...v1.22.5) (2023-09-02)


### Bug Fixes

* **layout:** fixed notification component ([fe2a344](https://gitlab.com/joshua_keyz/ping-ui/commit/fe2a3444a8a442a04c2bcc019490c99f3b21a7d1))

## [1.22.4](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.22.3...v1.22.4) (2023-09-02)


### Bug Fixes

* **layout:** fixed notification ([37e873d](https://gitlab.com/joshua_keyz/ping-ui/commit/37e873d95d4411efd96c89bc98df7d65772cdc18))

## [1.22.3](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.22.2...v1.22.3) (2023-09-02)


### Bug Fixes

* **table:** fixed styling ([e246366](https://gitlab.com/joshua_keyz/ping-ui/commit/e24636696422b0a711858dde1b4a61b35ed3bed3))

## [1.22.2](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.22.1...v1.22.2) (2023-09-02)


### Bug Fixes

* **layout:** fixed the styling ([0fc76a0](https://gitlab.com/joshua_keyz/ping-ui/commit/0fc76a096c053e9f2212336f66646965cb5316f4))

## [1.22.1](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.22.0...v1.22.1) (2023-09-02)


### Bug Fixes

* **table:** made table a bit responsive ([d3a2214](https://gitlab.com/joshua_keyz/ping-ui/commit/d3a22145db9ecdf47f02ca6cf3bdf3bbbc261c38))

# [1.22.0](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.21.0...v1.22.0) (2023-08-28)


### Features

* **logo:** fixed logo component ([681cc69](https://gitlab.com/joshua_keyz/ping-ui/commit/681cc6991f84d161ccc0ec42010373f3752b3f62))

# [1.21.0](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.20.20...v1.21.0) (2023-08-28)


### Features

* added some changes ([91f8264](https://gitlab.com/joshua_keyz/ping-ui/commit/91f82640a9fc4f4b128b3754e65c0142be1e5ae3))

## [1.20.20](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.20.19...v1.20.20) (2023-08-28)


### Bug Fixes

* fixed jenkins ([69cccf5](https://gitlab.com/joshua_keyz/ping-ui/commit/69cccf526e114fed8c871571dbd0dfebdae57481))

## [1.20.19](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.20.18...v1.20.19) (2023-08-28)


### Bug Fixes

* fixed jenkins file ([eb84ba6](https://gitlab.com/joshua_keyz/ping-ui/commit/eb84ba686066637dd384d9464b644b14a4151e2b))

## [1.20.18](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.20.17...v1.20.18) (2023-08-28)


### Bug Fixes

* fixed jenkins ([373bbb1](https://gitlab.com/joshua_keyz/ping-ui/commit/373bbb1e09784fecc28e727557f4fa59c7f96da3))
* fixed jenkins file ([826a9e6](https://gitlab.com/joshua_keyz/ping-ui/commit/826a9e65a589a9f4a06367582fb3da08a30d2451))

## [1.20.17](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.20.16...v1.20.17) (2023-08-07)


### Bug Fixes

* **select:** fixed the select component ([4c2ea6d](https://gitlab.com/joshua_keyz/ping-ui/commit/4c2ea6d28fd7a0d0654ac2eae703bc0836954b31))

## [1.20.16](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.20.15...v1.20.16) (2023-08-07)


### Bug Fixes

* **table:** fixed table rendition ([276ae38](https://gitlab.com/joshua_keyz/ping-ui/commit/276ae38b031ac4f251bf375ece8b652a42ef750c))

## [1.20.15](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.20.14...v1.20.15) (2023-08-07)


### Bug Fixes

* **table:** added some fine-tuning ([f9f1be1](https://gitlab.com/joshua_keyz/ping-ui/commit/f9f1be13c2e43d3bdad6a9fe923c0578d05ca5f3))

## [1.20.14](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.20.13...v1.20.14) (2023-08-07)


### Bug Fixes

* **table:** fixed rendition of table ([718ac90](https://gitlab.com/joshua_keyz/ping-ui/commit/718ac903675a92388f90483a29177bb2a5150004))

## [1.20.13](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.20.12...v1.20.13) (2023-08-06)


### Bug Fixes

* **table:** fixed pagination on datatable and also loading ([2a83cbd](https://gitlab.com/joshua_keyz/ping-ui/commit/2a83cbdd978d3f4380160575f806ea9bd50e0a3f))

## [1.20.12](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.20.11...v1.20.12) (2023-08-06)


### Bug Fixes

* **table:** fixed pagination issue ([3d34ed2](https://gitlab.com/joshua_keyz/ping-ui/commit/3d34ed27d4374ea9f2a693272a7379195923ada4))

## [1.20.11](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.20.10...v1.20.11) (2023-08-06)


### Bug Fixes

* **table:** fixed rendition of the table ([b5e837f](https://gitlab.com/joshua_keyz/ping-ui/commit/b5e837fd42a7092c1077c5eb6e08e720d2a9a0c9))

## [1.20.10](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.20.9...v1.20.10) (2023-08-06)


### Bug Fixes

* **select:** added z-index ([0f45afb](https://gitlab.com/joshua_keyz/ping-ui/commit/0f45afbe471bf9fe27a6663583e2e6f30002a519))

## [1.20.9](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.20.8...v1.20.9) (2023-08-06)


### Bug Fixes

* **input:** added placeholder to the input ([0792df4](https://gitlab.com/joshua_keyz/ping-ui/commit/0792df4afe8452a8073187f1cabf0978bb2aaea1))

## [1.20.8](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.20.7...v1.20.8) (2023-08-06)


### Bug Fixes

* **layout:** added notification ([5e36ef5](https://gitlab.com/joshua_keyz/ping-ui/commit/5e36ef573cc46f6c8a3f3e40c82e3cc43c6da947))

## [1.20.7](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.20.6...v1.20.7) (2023-08-06)


### Bug Fixes

* added some data changes ([ab44b6c](https://gitlab.com/joshua_keyz/ping-ui/commit/ab44b6cbb8185a9f470967b4cd8f77cbbfa543a2))
* added some data changes ([ec6a2cc](https://gitlab.com/joshua_keyz/ping-ui/commit/ec6a2cc5030d904d31476c9eabe9549618100f80))

## [1.20.6](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.20.5...v1.20.6) (2023-08-06)


### Bug Fixes

* fixed failing builds ([533811f](https://gitlab.com/joshua_keyz/ping-ui/commit/533811f338466aaac63e48844c092afc80d11591))

## [1.20.5](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.20.4...v1.20.5) (2023-08-06)


### Bug Fixes

* fixed failing builds ([868522f](https://gitlab.com/joshua_keyz/ping-ui/commit/868522fc59fc2df822f21df4e5455d3e4c7931c9))

## [1.20.4](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.20.3...v1.20.4) (2023-08-06)


### Bug Fixes

* fixed failing builds ([757bb7b](https://gitlab.com/joshua_keyz/ping-ui/commit/757bb7b8c519d0b7986f476c662d58864cf8c9ec))

## [1.20.3](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.20.2...v1.20.3) (2023-08-06)


### Bug Fixes

* fixed failing builds ([29155b6](https://gitlab.com/joshua_keyz/ping-ui/commit/29155b6bdcf88113d44000ab77614222e313a3cd))

## [1.20.2](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.20.1...v1.20.2) (2023-08-06)


### Bug Fixes

* fixed code build failures ([543596c](https://gitlab.com/joshua_keyz/ping-ui/commit/543596c95c86f5a38e89fd9ee3ff4d5b19519206))
* fixed failing build ([7320e97](https://gitlab.com/joshua_keyz/ping-ui/commit/7320e97305a024b3f68e183a350f7c63ca4a3443))

## [1.20.1](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.20.0...v1.20.1) (2023-08-06)


### Bug Fixes

* fixed failing build ([29fa100](https://gitlab.com/joshua_keyz/ping-ui/commit/29fa100f617f452e07532b829acf573aa135a1da))

# [1.20.0](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.19.4...v1.20.0) (2023-08-06)


### Features

* **layout:** added notifications to the pui-layout ([844a6b1](https://gitlab.com/joshua_keyz/ping-ui/commit/844a6b11988b938f2f9723112eff400189724880))

## [1.19.4](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.19.3...v1.19.4) (2023-08-03)


### Bug Fixes

* **table:** fixed empty templating ([b273ea9](https://gitlab.com/joshua_keyz/ping-ui/commit/b273ea90935fad47c0ebfab55177d403b0370536))

## [1.19.3](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.19.2...v1.19.3) (2023-08-03)


### Bug Fixes

* **table:** removed container ([b6b5a61](https://gitlab.com/joshua_keyz/ping-ui/commit/b6b5a6131e281a06fb9e8dd989e8004e5b192eb1))

## [1.19.2](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.19.1...v1.19.2) (2023-08-03)


### Bug Fixes

* **table:** removed contianer from table ([6dd8308](https://gitlab.com/joshua_keyz/ping-ui/commit/6dd83082e3d537d3be38dfd5a4f523f08f6b78e7))

## [1.19.1](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.19.0...v1.19.1) (2023-08-03)


### Bug Fixes

* **layout:** added scrollable content for desktop ([ed760bd](https://gitlab.com/joshua_keyz/ping-ui/commit/ed760bd38395dbeba11e88f1e9129dfdf2afe6e1))
* **table:** removed the max width from the table and paddings ([01c6582](https://gitlab.com/joshua_keyz/ping-ui/commit/01c65828ff4c6f03a4c3c752f93c17172275e84f))

# [1.19.0](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.18.0...v1.19.0) (2023-08-01)


### Bug Fixes

* **spinner:** fixed failing build ([8e17768](https://gitlab.com/joshua_keyz/ping-ui/commit/8e1776819b467f4fdfd93860fe6159e53e1bd74d))


### Features

* **spinner:** added spinner component ([0d5f302](https://gitlab.com/joshua_keyz/ping-ui/commit/0d5f30247dd423eec3f6f32d42292582ac4778ce))

# [1.18.0](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.17.0...v1.18.0) (2023-08-01)


### Features

* **table:** added datatable and spinner ([874d46d](https://gitlab.com/joshua_keyz/ping-ui/commit/874d46db889e77990da1d4e25c482ff562573793))

# [1.17.0](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.16.3...v1.17.0) (2023-07-25)


### Features

* added layout component and fixed select component ([1122e1b](https://gitlab.com/joshua_keyz/ping-ui/commit/1122e1b08eac2f1017a7823f5d3ff9157fb8cbbd))

## [1.16.3](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.16.2...v1.16.3) (2023-07-23)


### Bug Fixes

* fixed absolute positioning of the profile ([06f3a14](https://gitlab.com/joshua_keyz/ping-ui/commit/06f3a146ce2ac4fc98f2a0eada8e4a6341d78ba5))
* fixed profile pic not displaying issue ([72e2f35](https://gitlab.com/joshua_keyz/ping-ui/commit/72e2f3594c79ddaaa5529c09e9b7dba607975de7))

## [1.16.2](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.16.1...v1.16.2) (2023-07-23)


### Bug Fixes

* reduced the width of the sidebar ([3daec0b](https://gitlab.com/joshua_keyz/ping-ui/commit/3daec0ba2b1bba4c4b0dd8ec7584be9ae5e54775))

## [1.16.1](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.16.0...v1.16.1) (2023-07-23)


### Bug Fixes

* added profile-placeholder.png ([204a529](https://gitlab.com/joshua_keyz/ping-ui/commit/204a529050e0fb38291f6b63988a99b5a0cd7d78))
* added profile-placeholder.png ([76a620a](https://gitlab.com/joshua_keyz/ping-ui/commit/76a620af1cacf87b46c18860752a043e38fe09d0))

# [1.16.0](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.15.2...v1.16.0) (2023-07-23)


### Features

* **profile:** added profile component ([bd93e1e](https://gitlab.com/joshua_keyz/ping-ui/commit/bd93e1ec055916528b770af72e58e07bcb5296a7))

## [1.15.2](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.15.1...v1.15.2) (2023-07-23)


### Bug Fixes

* added router testing ([e108c16](https://gitlab.com/joshua_keyz/ping-ui/commit/e108c163df759ad23af47f71451784f6c99e9c4f))
* fixed failing builds ([04b3e75](https://gitlab.com/joshua_keyz/ping-ui/commit/04b3e75eac90b0a4ee4a46d13902cc0d47ce53aa))
* fixed failing builds ([2eca677](https://gitlab.com/joshua_keyz/ping-ui/commit/2eca6770957fbdeb3dd4cd551ab326c9147e6dc1))
* removed router module from ping-ui ([85d8925](https://gitlab.com/joshua_keyz/ping-ui/commit/85d892543dd6bcaa3d5d14b8943b67389a41a889))

## [1.15.1](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.15.0...v1.15.1) (2023-07-23)


### Bug Fixes

* changed export order of public api ([e3b26e4](https://gitlab.com/joshua_keyz/ping-ui/commit/e3b26e4e73e4703a6feeaa6e5c1911aa776f3aa8))

# [1.15.0](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.14.0...v1.15.0) (2023-07-23)


### Features

* **layout:** added layout component ([af36bfc](https://gitlab.com/joshua_keyz/ping-ui/commit/af36bfca9bdf1c41afb5cd48037e2f7da0d36bb1))

# [1.14.0](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.13.11...v1.14.0) (2023-07-19)


### Features

* **link:** added secondary variant to pui-link component ([368fd12](https://gitlab.com/joshua_keyz/ping-ui/commit/368fd12e040dbbb32d993267891c91ffc3927542))

## [1.13.11](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.13.10...v1.13.11) (2023-07-19)


### Bug Fixes

* adjusted styling for container ([e7a4e54](https://gitlab.com/joshua_keyz/ping-ui/commit/e7a4e54c5502cf2855fe3e19dbf90c0214368649))

## [1.13.10](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.13.9...v1.13.10) (2023-07-19)


### Bug Fixes

* **button:** fixed styling issues ([7301709](https://gitlab.com/joshua_keyz/ping-ui/commit/7301709d03dd889f134441f1d4846feb668a71e9))

## [1.13.9](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.13.8...v1.13.9) (2023-07-19)


### Bug Fixes

* **form-field:** fixed form field error handling ([3b8e7cf](https://gitlab.com/joshua_keyz/ping-ui/commit/3b8e7cf982b79c2ba9711723bbafcd3514f30000))

## [1.13.8](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.13.7...v1.13.8) (2023-07-19)


### Bug Fixes

* **form-field:** added other inputs to the storybook documentation ([cc9be88](https://gitlab.com/joshua_keyz/ping-ui/commit/cc9be8883d98ab42d11036ebb4062d9c0dc66d12))

## [1.13.7](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.13.6...v1.13.7) (2023-07-19)


### Bug Fixes

* **form-field:** fixed form-field component ([08e85ec](https://gitlab.com/joshua_keyz/ping-ui/commit/08e85ec7800759d36199927ba322d6dee5caaa1d))

## [1.13.6](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.13.5...v1.13.6) (2023-07-18)


### Bug Fixes

* added colors to all the headings ([50d9f59](https://gitlab.com/joshua_keyz/ping-ui/commit/50d9f598bf07f053ca254b3a473aa29e7278d9ad))
* adjusted line height of the headings ([3e8047d](https://gitlab.com/joshua_keyz/ping-ui/commit/3e8047d9f717848cd3c84ae76ab9d8b50702db77))

## [1.13.5](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.13.4...v1.13.5) (2023-07-18)


### Bug Fixes

* added more display styles ([82f1345](https://gitlab.com/joshua_keyz/ping-ui/commit/82f1345f66a7d70f48a0d1e5a7ce24dadde26b42))

## [1.13.4](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.13.3...v1.13.4) (2023-07-18)


### Bug Fixes

* updated the grid ([a0d6361](https://gitlab.com/joshua_keyz/ping-ui/commit/a0d63617568ad0b3801b8c3c82677fb5ffb5220c))

## [1.13.3](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.13.2...v1.13.3) (2023-07-18)


### Bug Fixes

* fixed logo issues ([d40fe48](https://gitlab.com/joshua_keyz/ping-ui/commit/d40fe48104b826e54fa53f56d9f575dd33f533e0))

## [1.13.2](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.13.1...v1.13.2) (2023-07-18)


### Bug Fixes

* fixed issues with images not showing up ([25deff6](https://gitlab.com/joshua_keyz/ping-ui/commit/25deff673544d5c03e0084245e3d823b553b7641))

## [1.13.1](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.13.0...v1.13.1) (2023-07-18)


### Bug Fixes

* added logos ([eed7fd9](https://gitlab.com/joshua_keyz/ping-ui/commit/eed7fd9646481b136d038182240840e324e966e8))

# [1.13.0](https://gitlab.com/joshua_keyz/ping-ui/compare/v1.12.0...v1.13.0) (2023-07-18)


### Bug Fixes

* added automatically generated docs to button , form field and icon ([f3fa5e1](https://gitlab.com/joshua_keyz/ping-ui/commit/f3fa5e1929aa309de14dc3fec8642dbd123cfe30))
* added correct server address ([1c3ac25](https://gitlab.com/joshua_keyz/ping-ui/commit/1c3ac25e2eb36264d1c8b09457bfa97d608238a1))
* added gitlab access token ([c5adf95](https://gitlab.com/joshua_keyz/ping-ui/commit/c5adf95aa740cf72f3440d5b9fbb6953fa51eb22))
* added some changes ([aecade2](https://gitlab.com/joshua_keyz/ping-ui/commit/aecade218c6e1dc9c8d7d3b26a3216f48ae071f0))
* **app:** added ansible playbook ([fb0b73b](https://gitlab.com/joshua_keyz/ping-ui/commit/fb0b73b1217d31cd263c2d8c4e37485bc4d82942))
* **app:** fixing pipelines ([19482a5](https://gitlab.com/joshua_keyz/ping-ui/commit/19482a5ac47a4e185bbba8bf56227d6a9e2857e6))
* **app:** fixing pipelines ([b48d461](https://gitlab.com/joshua_keyz/ping-ui/commit/b48d461d85350607ba4ca8e495540f12efcf91bb))
* **app:** fixing pipelines ([43ac6b7](https://gitlab.com/joshua_keyz/ping-ui/commit/43ac6b70015ee631ef3ff1037d8fdb935e5af681))
* **button:** fixed broken pipelines ([264bc2f](https://gitlab.com/joshua_keyz/ping-ui/commit/264bc2f79265c3902b04be4bb588dadc22d8195c))
* change semantic release branch ([19fabcb](https://gitlab.com/joshua_keyz/ping-ui/commit/19fabcb700d43deb81dd223542f5b7edb26e5c14))
* fixed build failure ([9502290](https://gitlab.com/joshua_keyz/ping-ui/commit/9502290575d4181de702c371f745cdf0dc6506a7))
* fixed failing build ([8be7dcb](https://gitlab.com/joshua_keyz/ping-ui/commit/8be7dcb8a205cd3db4ee72225e7ab3812cc11124))
* fixed failing builds ([49aae6b](https://gitlab.com/joshua_keyz/ping-ui/commit/49aae6b4e7c1727ef16f0937c67ce2fe9218e420))
* fixed merge conflicts ([129ffc0](https://gitlab.com/joshua_keyz/ping-ui/commit/129ffc034659c645536a90aa8e5cc98f45602890))
* fixed merge conflicts ([ea36db5](https://gitlab.com/joshua_keyz/ping-ui/commit/ea36db53736f6cc993819d8272e29101da6a55c7))
* fixing broken pipelines ([88bceff](https://gitlab.com/joshua_keyz/ping-ui/commit/88bceff427c21d788ca010b4bcde772ae45d69cc))
* removed  documentation example templates ([cea49c7](https://gitlab.com/joshua_keyz/ping-ui/commit/cea49c7b56a2277a8fc3026b2690dbdc11533ac1))


### Features

* added assets ([29e7a07](https://gitlab.com/joshua_keyz/ping-ui/commit/29e7a07a289a58edfc829b30d408099f45ff6ce0))
* added input ([71b3e3b](https://gitlab.com/joshua_keyz/ping-ui/commit/71b3e3b650252f3956f0c95a3f0461807ad773a4))
* added post scripts ([3767c03](https://gitlab.com/joshua_keyz/ping-ui/commit/3767c03f885cc3020ddc1be18187060c6196fbfe))
* added post scripts ([53252c9](https://gitlab.com/joshua_keyz/ping-ui/commit/53252c94e47b7fa74a9571284ac997158d769662))
* **alert:** implemented alert module ([9a0bf6f](https://gitlab.com/joshua_keyz/ping-ui/commit/9a0bf6fcc655987270a94711bc7940f3ba4ee051))
* **button:** added button variants and button types ([fa018be](https://gitlab.com/joshua_keyz/ping-ui/commit/fa018be3ac4a62d406873993681fa137498a3477))
* **button:** added different variants and types to the button component ([6848859](https://gitlab.com/joshua_keyz/ping-ui/commit/684885951ae7f17d68f441338dcff07bd78d3ab8))
* **colors:** implemented colors and added documentation to storybook ([efc4366](https://gitlab.com/joshua_keyz/ping-ui/commit/efc4366743b14bdf1d825438baba3a70142159ef))
* **form-field:** added  attribute to the form-field ([7dd55e8](https://gitlab.com/joshua_keyz/ping-ui/commit/7dd55e8bfcc9dee961db059b0cbcd997086772d7))
* **form-field:** added form field to the library ([c9d1446](https://gitlab.com/joshua_keyz/ping-ui/commit/c9d144623cc5e869f34233ed5f6820a5341c7102))
* **form-field:** added type parameter ([3ae2d18](https://gitlab.com/joshua_keyz/ping-ui/commit/3ae2d188173acaa8dca391b9c111a6df44517339))
* **link:** implemented links ([7bc91b2](https://gitlab.com/joshua_keyz/ping-ui/commit/7bc91b2ff626a2a4b0247e25d4433634c8f30b0c))
* **logo:** implemented the logo component ([7906068](https://gitlab.com/joshua_keyz/ping-ui/commit/79060685e99ea22860cf0217d5af10b9fd7aa62d))
* **logo:** implemented the logo component ([d41225a](https://gitlab.com/joshua_keyz/ping-ui/commit/d41225a11fec0b5b6897ab9c194b362678a07d2f))

# [1.12.0](https://github.com/JoshuaKeys/ping-ui/compare/v1.11.0...v1.12.0) (2023-05-27)


### Features

* **button:** added simple button ([6f1484d](https://github.com/JoshuaKeys/ping-ui/commit/6f1484de45cca79ac5030a611bc1bc3b632f8662))

# [1.11.0](https://github.com/JoshuaKeys/ping-ui/compare/v1.10.0...v1.11.0) (2023-05-27)


### Features

* **introduction:** added title of the introduction ([39f816a](https://github.com/JoshuaKeys/ping-ui/commit/39f816afa9863696bcc11080a5a9b6758aacfe1e))

# [1.10.0](https://github.com/JoshuaKeys/ping-ui/compare/v1.9.8...v1.10.0) (2023-05-27)


### Bug Fixes

* **ansible:** testing ansible ([6686bfc](https://github.com/JoshuaKeys/ping-ui/commit/6686bfc88f9f69f84851a4e398255c53875013d1))
* **ansible:** testing ansible ([bdf901d](https://github.com/JoshuaKeys/ping-ui/commit/bdf901d231a5947750af38301cf779c728b79092))
* **setup:** removed storybook static ([a719f8a](https://github.com/JoshuaKeys/ping-ui/commit/a719f8af17cd26ecafe795750ce5f39d3638f0b1))


### Features

* **cicd:** finished ci/cd ([cf35e6d](https://github.com/JoshuaKeys/ping-ui/commit/cf35e6d78bdfa20e96d0d6489ff72e183b81ebd1))
* **cicd:** finished ci/cd ([64bf6b1](https://github.com/JoshuaKeys/ping-ui/commit/64bf6b1a58bb5d9840789a2662b1b1bf2cceb477))

## [1.9.8](https://github.com/JoshuaKeys/ping-ui/compare/v1.9.7...v1.9.8) (2023-05-23)


### Bug Fixes

* **release:** fixed the release pipeline ([31f39dd](https://github.com/JoshuaKeys/ping-ui/commit/31f39dd7966cc1e5112180aa63aebe081bf3e7b5))

## [1.9.7](https://github.com/JoshuaKeys/ping-ui/compare/v1.9.6...v1.9.7) (2023-05-23)


### Bug Fixes

* **release:** fixed the release pipeline ([562def0](https://github.com/JoshuaKeys/ping-ui/commit/562def09e4c73f598759064c7563c3287c4abc54))
* **release:** fixed the release pipeline ([e33d474](https://github.com/JoshuaKeys/ping-ui/commit/e33d474fbc0683aac01bdda01be427450f6dc866))
* **release:** fixed the release pipeline ([fd10e83](https://github.com/JoshuaKeys/ping-ui/commit/fd10e83b3b79868589f12b15fb9bd030af83d793))

## [1.9.6](https://github.com/JoshuaKeys/ping-ui/compare/v1.9.5...v1.9.6) (2023-05-23)


### Bug Fixes

* **release:** fixed the release pipeline ([3b98c25](https://github.com/JoshuaKeys/ping-ui/commit/3b98c255386b41c8ca14f7893ef6f022700e0922))
* **release:** fixed the release pipeline ([3cba3b9](https://github.com/JoshuaKeys/ping-ui/commit/3cba3b9dbd4a7463f42e78406bace2396140f94c))

## [1.9.5](https://github.com/JoshuaKeys/ping-ui/compare/v1.9.4...v1.9.5) (2023-05-23)


### Bug Fixes

* **release:** fixed the release pipeline ([9a0084f](https://github.com/JoshuaKeys/ping-ui/commit/9a0084f92cb82ca2b33e3bb9b90636558b6a5763))

## [1.9.4](https://github.com/JoshuaKeys/ping-ui/compare/v1.9.3...v1.9.4) (2023-05-23)


### Bug Fixes

* **release:** fixed the release pipeline ([02a7510](https://github.com/JoshuaKeys/ping-ui/commit/02a7510dba874eec1c2aaa4181bee4fda66eb886))

## [1.9.3](https://github.com/JoshuaKeys/ping-ui/compare/v1.9.2...v1.9.3) (2023-05-22)


### Bug Fixes

* **release:** fixed the release pipeline ([675dc3f](https://github.com/JoshuaKeys/ping-ui/commit/675dc3f1e3da2fe8221bed86acea258b5dd85963))

## [1.9.2](https://github.com/JoshuaKeys/ping-ui/compare/v1.9.1...v1.9.2) (2023-05-22)


### Bug Fixes

* **release:** doing pipeline work ([7257e8b](https://github.com/JoshuaKeys/ping-ui/commit/7257e8ba11c4f3391102bac674153a330ccc04f7))

## [1.9.1](https://github.com/JoshuaKeys/ping-ui/compare/v1.9.0...v1.9.1) (2023-05-22)


### Bug Fixes

* **deployment:** fixed pinglink server ([8d96e3b](https://github.com/JoshuaKeys/ping-ui/commit/8d96e3b4696e30edd65dac639bfd342de9887c0a))

# [1.9.0](https://github.com/JoshuaKeys/ping-ui/compare/v1.8.0...v1.9.0) (2023-05-22)


### Features

* **button:** removed large and secondary variants ([6820c67](https://github.com/JoshuaKeys/ping-ui/commit/6820c67b967ba8e3e3b25d39d559aad430355f45))
* **button:** removed the small button variant ([131d44f](https://github.com/JoshuaKeys/ping-ui/commit/131d44f047b428ac2418f5866ac5d329c1367cbe))
* **testing:** added somethings ([c72c023](https://github.com/JoshuaKeys/ping-ui/commit/c72c02399ece912346c7d60fa7ffca7eb6ed4e7b))

# [1.8.0](https://github.com/JoshuaKeys/ping-ui/compare/v1.7.0...v1.8.0) (2023-05-22)


### Features

* **testing:** welcome to ui world ([5d84102](https://github.com/JoshuaKeys/ping-ui/commit/5d84102571173754642b77f95984f43c31d88203))

# [1.7.0](https://github.com/JoshuaKeys/ping-ui/compare/v1.6.0...v1.7.0) (2023-05-22)


### Features

* **testing:** welcome to ui world ([0169756](https://github.com/JoshuaKeys/ping-ui/commit/0169756a14a18b3d2e226bbf964c78fa667840a5))

# [1.6.0](https://github.com/JoshuaKeys/ping-ui/compare/v1.5.0...v1.6.0) (2023-05-22)


### Features

* **testing:** changed the docs ([e7dd538](https://github.com/JoshuaKeys/ping-ui/commit/e7dd538cc0c0d2bce2e3c0c3ee65ef58ea20780b))

# [1.5.0](https://github.com/JoshuaKeys/ping-ui/compare/v1.4.0...v1.5.0) (2023-05-22)


### Bug Fixes

* **app:** updated building process ([a5f11f5](https://github.com/JoshuaKeys/ping-ui/commit/a5f11f567f5902f0b9a38316e01f81483efa1850))


### Features

* **release:** fixed pipelines ([9acf3b4](https://github.com/JoshuaKeys/ping-ui/commit/9acf3b472967ec2f2dac31e4375ce853d076480b))

# [1.4.0](https://github.com/JoshuaKeys/ping-ui/compare/v1.3.0...v1.4.0) (2023-05-22)


### Features

* **releases:** fixed broken pipelines ([01df7db](https://github.com/JoshuaKeys/ping-ui/commit/01df7dbf89309b79ff61fae05f82194a6b6a0ce5))
* **storybook:** added storybook and storybook deployment ([5636eae](https://github.com/JoshuaKeys/ping-ui/commit/5636eaeeabb9b40198b7e5b234893943b2ab86b9))

# [1.3.0](https://github.com/JoshuaKeys/ping-ui/compare/v1.2.0...v1.3.0) (2023-05-22)


### Features

* **release:** fixed pipelines failing ([c7dcd88](https://github.com/JoshuaKeys/ping-ui/commit/c7dcd8866a392e7c8d2a177908e620403de96a29))

# [1.2.0](https://github.com/JoshuaKeys/ping-ui/compare/v1.1.0...v1.2.0) (2023-05-22)


### Features

* **release:** updated release ([c822273](https://github.com/JoshuaKeys/ping-ui/commit/c822273039bc4632d26f6ed14e9e7d54ebbaa9f7))

# [1.1.0](https://github.com/JoshuaKeys/ping-ui/compare/v1.0.0...v1.1.0) (2023-05-22)


### Features

* **ping-ui:** change the project's name ([1e4af44](https://github.com/JoshuaKeys/ping-ui/commit/1e4af44d942d66811f2cd12ef2f3a000392883e1))

# 1.0.0 (2023-05-22)


### Bug Fixes

* **release:** added semantic release ([4a63786](https://github.com/JoshuaKeys/ping-ui/commit/4a63786bf7257448acc9edd21ab05072fd0e2885))


### Features

* **app:** added linting ([9238fda](https://github.com/JoshuaKeys/ping-ui/commit/9238fda2a16cdd8e076f403e9d9676e0005618bf))
* **ping-ui:** added semantic release ([3ce459e](https://github.com/JoshuaKeys/ping-ui/commit/3ce459e698fcb4f9bc4071d7f3443d7051a4a4d5))
* **release:** adding releases ([44a6a98](https://github.com/JoshuaKeys/ping-ui/commit/44a6a988ce91d19e0a1d7eedae1bb665825a6f57))
* **release:** fixed semantic release issue ([d67ed8c](https://github.com/JoshuaKeys/ping-ui/commit/d67ed8ca4c05e2c1d5657e558232caacf82be87c))
* **setup:** added semantic release ([43cf664](https://github.com/JoshuaKeys/ping-ui/commit/43cf664b88db1377d32c379885f7f13eec053753))
* **setup:** added semantic release to origin/master ([6251b06](https://github.com/JoshuaKeys/ping-ui/commit/6251b062c67a93d9a9c1675d3c91ef8069c11e5f))
* **setup:** fixing broken builds ([14f4e4b](https://github.com/JoshuaKeys/ping-ui/commit/14f4e4bd8e286c6a4adc53b8705f0668c576d703))
* **setup:** releasing ([cdf45d8](https://github.com/JoshuaKeys/ping-ui/commit/cdf45d86f9b2299f41664ff3afaa43bb451f9e2f))
