#!/bin/bash

echo "Convert scss variables to js object"

SCSSVariablesPath="projects/ping-ui/src/lib/public/assets"

GeneratedFolder="projects/ping-ui/src/stories/design-tokens/"
GeneratedFileContent=$'/* tslint:disable */\nexport const SCSS_VARIABLES = \n{'

convert_scss_vars_to_json() {
    scss_file_path=$1
    generated_json_file_path=$2
    arr_output=$3

    echo "Converting scss variables from ${css_file_path} to ${generated_json_file_path}..."
    if [ "$arr_output" = "true" ]
    then
        npm run sass-export -- "$scss_file_path" -o "$generated_json_file_path" -a
    else 
        npm run sass-export "$scss_file_path" -o "$generated_json_file_path"    
    fi

    if [ $? != 0 ];
    then
        echo "Converting process was failed"
        GeneratedFileContent="${GeneratedFileContent} []"
        return 1
    fi

    

    echo "Copy to buffer from ${generated_json_file_path}..."
    buffer=`cat "$PWD/${generated_json_file_path}"`
    GeneratedFileContent="${GeneratedFileContent} ${buffer}"
    return 0
}

echo "Typography:"
GeneratedFileContent="${GeneratedFileContent} typography:"
convert_scss_vars_to_json "${SCSSVariablesPath}/typography/_tokens.scss" "${GeneratedFolder}/_typography-scss-variables.generated.json" true

echo "Sizes:"
GeneratedFileContent="${GeneratedFileContent}, sizes:"
convert_scss_vars_to_json "${SCSSVariablesPath}/_sizes.scss" "${GeneratedFolder}/_sizes-scss-variables.generated.json" true


echo "Colors:"
GeneratedFileContent="${GeneratedFileContent}, colors:"
convert_scss_vars_to_json "${SCSSVariablesPath}/_colors.scss" "${GeneratedFolder}/_colors-scss-variables.generated.json" true


echo "${GeneratedFileContent}};" > "$PWD/${GeneratedFolder}/sass-variables.generated.ts"
